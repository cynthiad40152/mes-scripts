#!/bin/bash

read -p "Quelle est votre poids en kg?" poids

read -p "Quelle est votre taille en mètres?" taille

taille_totale=$(bc <<< "$taille*$taille") 

resultat=$(bc <<< "$poids/$taille_totale")

echo "Votre IMC est de $resultat"

if [[ "$resultat" -le 19 ]] 
then
	echo "Vous etes maigre il faut manger un peu plus!"

elif [[ "$resultat" -gt 19 && "$resultat" -le 25 ]] 
then
	echo "Vous avez un poids normal"

elif [[ "$resultat" -gt 25 && "$resultat" -le 30 ]]
then
	echo "Vous êtes en surpoids"

else 
	echo "Vous êtes obèse!"
fi
